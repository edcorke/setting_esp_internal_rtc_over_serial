# setting_esp_internal_rtc_over_serial

Code to set the Unix epoch time of an ESP32s internal RTC over serial.

The Python script can be used to get the current Unix epoch/ use a website such as 'https://www.epochconverter.com/'


Source: https://github.com/espressif/arduino-esp32/issues/3641
