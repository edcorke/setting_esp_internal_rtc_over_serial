#define TZ_INFO "GMTGMT-1,M3.4.6/02:00:00,M10.5.6/02:00:00"    // http://www.gnu.org/software/libc/manual/html_node/TZ-Variable.html
#define PRINT_DELAY 5

#include <sys/time.h>
#include <Ticker.h>
Ticker time_print;

int setUnixtime(int32_t unixtime) {
  timeval epoch = {unixtime, 0};
  return settimeofday((const timeval*)&epoch, 0);
}

void timePrint() {
  struct tm now;
  getLocalTime(&now,0);
  if (now.tm_year >= 117) Serial.println(&now, "%B %d %Y %H:%M:%S (%A)");
}

void setup() {
  Serial.begin(115200);

  // Setting Unix epoch timezone & disable serial timeout so it can wait for a time to be sent
  setenv("TZ", TZ_INFO, 1);
  tzset(); // Assign the local timezone from setenv
  time_print.attach(PRINT_DELAY, timePrint); // Schedule the timeprint function
  disableCore0WDT(); //turn off WDT since we will have a blocking task
  Serial.setTimeout(LONG_MAX); //don't timeout
}

void loop() {
  // Wait for input from serial (note that this is blocking)
  Serial.println("Waiting to receive Unix epoch time over serial (ending with \\n)");
  String timeStr = Serial.readStringUntil('\n');
  uint32_t timeInt32 = atoi(timeStr.c_str());
  if (timeInt32 < 1480000000) {
    Serial.println("Please enter a Unix epoch time after 01/01/2017");
  } else {
    setUnixtime(timeInt32);
  }
}
